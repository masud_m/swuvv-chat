var passport = require('passport');
var FacebookStrategy = require('passport-facebook');

var User = require('../models/user');
var init = require('./init');
var vals = require('../const');

passport.use(new FacebookStrategy({
	clientID: vals.clientID,
	clientSecret: vals.clientSecret,
	callbackURL: vals.callbackURL,
	profileFields: ['id', 'displayName', 'emails', 'age_range']
	},
  	//facebook sends back the tokens and progile info
  	function(token, tokenSecret, profile, done) {
		if (profile._json.age_range.min == 13) {
			done(null, false, { message : 'You must be 18 or over to use this site (logged into ' + profile.displayName + ')' }); //too young
			return false;
		}

		var searchQuery = {
			serviceID: profile.id
		};

		var updates = {
			provider: profile.provider,
			name: profile.displayName,
			serviceID: profile.id,
			email: profile.emails[0].value
		};

		var options = {
			upsert: true
		};

		// update the user if s/he exists or add a new user
		User.findOneAndUpdate(searchQuery, updates, options, function(err, user) {
			if(err) {
				return done(err);
			} else {
				return done(null, user);
			}
		});
	}
));
// serialize user into the session
init();

module.exports = passport;