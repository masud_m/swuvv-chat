//require variables
var express = require('express');
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var moment = require('moment');
var passport = require('passport');
var passportFacebook = require('./auth/facebook');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var ss = require('socket.io-stream');

//models
var Contacts = require('./models/contacts');
var Msg = require('./models/messages');
var User = require('./models/user');

//other variables 
var storage = multer.diskStorage({
	destination: function(req, file, cb) {
		if (file.fieldname == 'profile-picture') {
			cb(null, './public/uploads/profile-picture/');
		} else if (file.fieldname == 'messages') {
			cb(null, './public/uploads/messages/');
		} else {
			cb(null, './public/uploads/');
		}
	},
	filename: function(req, file, cb) {
		var name = createRandomName(file.originalname, '');
		cb(null, name);
	}
});

function fileFilter (req, file, cb){
	if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/jpg' || file.mimetype == 'image/png' || file.mimetype == 'image/gif') {
		cb(null, true);
	} else {
		cb(null, false);
	}
}
var upload = multer({storage: storage, dest: "./uploads/", fileFilter: fileFilter, limits: { fileSize: 5 * 1024 * 1024 }});

//configure
var sessionMiddleware = session({
	resave: true,
	saveUninitialized: true,
    name: "socketchat",
    secret: "socketchatsecret",
    store: new (require("connect-mongo")(session))({
        url: "mongodb://localhost/socketchat"
    })
});
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public')); //where assests are held
//app.use(logger('dev')); //log requests
app.use(bodyParser.json({limit: '15mb'}));
app.use(bodyParser.urlencoded({limit: '15mb', extended: true}));
app.use(cookieParser());
app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//---- ROUTES ----//
//these routes do not need the user to be logged in
//login
app.get('/login',function(req, res) {
	if (req.user !== undefined) {
		res.redirect('/');
	}
	res.render('login', { loginError: req.flash('error')[0] });
});

//facebook auth
app.get('/auth/facebook', passportFacebook.authenticate('facebook', { scope: ['email'] }));
app.get('/auth/facebook/callback', passportFacebook.authenticate('facebook', { failureRedirect: '/login', failureFlash : true}), function(req, res) {
	// Successful authentication
	res.redirect('/');
});

//these you do need to be logged in for
//route middleware to make sure logged in
var isLoggedIn = function(req, res, next) {
  //if user is authenticated in the session, carry on
	//if (req.isAuthenticated()) {
	if (req.user !== undefined) {
		if (req.user.info.bio === undefined && req.url != '/setup' && req.url != '/settings/bio' && req.url != '/settings/gender' && req.url != '/upload/profile-picture') {
			res.redirect('/setup');
			return false;
		}
        next();
    } else {
		//if they aren't redirect them to the home page
		res.redirect('/login');
	}
	
};
app.use(isLoggedIn);

//homepage
app.get('/',function(req, res) { 
    getContacts(req, res, function(err, user, contacts, photos) {
        if(is_mobile(req)) {
            res.render('mobile/contacts-mobile', {user: user, contacts: contacts, contactsProfile: photos});
        } else {
            res.render('index', {user: user, contacts: contacts, contactsProfile: photos});
        }
    });
});
//settings
app.get('/settings',function(req, res) { 
	if(is_mobile(req)) {
		res.render('mobile/settings-mobile', {user: req.user});
	} else {
		getContacts(req, res, function(err, user, contacts, photos) {
			res.render('settings', {user: user, contacts: contacts, contactsProfile: photos});
		});
	}
});
//initial setup
app.get('/setup', function(req, res) {
	if(is_mobile(req)) {
		res.render('mobile/setup-mobile', {user: req.user});
	} else {
		res.render('setup', {user: req.user});
	}
});
//Make new friends
app.get('/new',function(req, res) { 
	var u = User.find().where("_id").ne(req.user._id);
	u.exec(function(err, friends) {
		if(is_mobile(req)) {
			res.render('mobile/newfriends-mobile', {user: req.user, friends: friends});
		} else {
			getContacts(req, res, function(err, user, contacts, photos) {
				if (err) throw err;
				res.render('newfriends', {user: user, contacts: contacts, contactsProfile: photos, friends: friends});
			});
		}
	});   
});
//messages
app.get('/messages/:room', function(req, res) {
	var verify = Contacts.find({people: req.user._id, roomName: req.params.room}).limit(1).lean();
	verify.exec(function(err, docs) {
		if (err) throw err;
        if (docs.length === 0) {
			//not a contact
			res.redirect('/');
            return false;
		}
		docs = docs[0];
		friendId = docs.people[0];
		if (friendId == req.user._id) {
			friendId = docs.people[1];
		}
		
		var u = User.find({_id: friendId}).limit(1).lean();
		u.exec(function(err, friend) {
			if (err) throw err;
			if (friend.length === 0) {
				//not a contact
				res.redirect('/');
				return false;
			}
			friend = friend[0];
			
			if(is_mobile(req)) {
				if (docs.confirmed === false) {
					//they may need to confirm
					if (docs.people[0] == req.user._id) {
						res.render('mobile/confirmFriend-mobile', {type: 'sent', contactId: docs._id, user: req.user});
					} else if (docs.people[1] == req.user._id) {
						res.render('mobile/confirmFriend-mobile', {type: 'recieved', contactId: docs._id, user: req.user, friend: friend});
					} else {
						res.redirect('/error/Server%20Error');
                        return false;
					}
				} else {
					res.render('mobile/messages-mobile', {room: req.params.room, user: req.user, friend: friend});
				}
			} else {
				getContacts(req, res, function(err, user, contacts, photos) {
					if (docs.confirmed === false) {
						//they may need to confirm
						if (docs.people[0] == req.user._id) {
							res.render('confirmFriend', {type: 'sent', contactId: docs._id, user: req.user, contacts: contacts, contactsProfile: photos});
						} else if (docs.people[1] == req.user._id) {
							res.render('confirmFriend', {type: 'recieved', contactId: docs._id, user: req.user, contacts: contacts, contactsProfile: photos, friend: friend});
						} else {
							res.redirect('/error/Server%20Error');
                            return false;
						}
					} else {
						res.render('messages', {room: req.params.room, user: user, contacts: contacts, contactsProfile: photos, friend: friend});
					}
				});
			}
		});
	});
});
app.get('/error/:error', function(req, res) {
    res.render('Error', {message: req.params.error});
});
//make sure last GET so redirect on error
app.get('*', function(req, res) {
    res.redirect('/error/Page%20Not%20Found');
    return false;
});

//POST requests
app.post('/upload/profile-picture', upload.single('profile-picture'), function(req, res) {
	var query = {'_id':req.user._id};
	User.update(query, { $set: { profilePicture: req.file.filename }}, function(err, doc) {
		if (err) return res.send(500, { error: err });
		return res.send(["confirmed", req.file.filename]);
	});
});
app.post('/settings/bio', function(req, res) {
	//cl('bio: ' + req.body.bio);
	var query = {'_id':req.user._id};
	bio = req.body.bio;
	if (bio.length < 401) {
		User.update(query, { $set: { 'info.bio': bio }}, function(err, doc) {
			if (err) return res.send(500, { error: err });
			return res.send(["confirmed"]);
		});
	} else {
		if (err) return res.send(500, { error: err });
	}
	
});
app.post('/settings/age', function(req, res) {
	dobM = moment(req.body.dob, "DD-MMM-YYYY");
	dobMin = moment(moment().subtract("18", 'years').calendar(), "MM/DD/YYYY");
	if (!dobM.isBefore(dobMin)) {
		return false;
	}
	var query = {'_id':req.user._id};
	User.update(query, { $set: { 'info.dob.value': req.body.dob, 'info.dob.view': req.body.dobView, 'info.dob.decade': req.body.decade}}, function(err, doc) {
		if (err) return res.send(500, { error: err });
		return res.send(["confirmed"]);
	});
});
app.post('/settings/gender', function(req, res) {
	var query = {'_id':req.user._id};
	User.update(query, { $set: { 'info.gender.value': req.body.gender, 'info.gender.view': req.body.genderView }}, function(err, doc) {
		if (err) return res.send(500, { error: err });
		return res.send(["confirmed"]);
	});
});
app.post('/settings/media', function(req, res) {
	var query = {'_id':req.user._id};
	User.update(query, 
				{ 
					$set: { 
						'info.media.view': req.body.mediaView, 
						'info.media.facebook': req.body.fb,
						'info.media.twitter': req.body.tw,
						'info.media.instagram': req.body.ig, 
						'info.media.snapchat': req.body.sc, 
						'info.media.youtube': req.body.yt, 
						'info.media.linkedin': req.body.li
					}
	}, function(err, doc) {
		if (err) return res.send(500, { error: err });
		return res.send(["confirmed"]);
	});
});
app.post('/friend/confirm', function(req, res) {
	var query = {_id: req.body.roomId, people: req.user._id};
	Contacts.update(query, { $set: { confirmed: true }}, function(err, doc) {
		if (err) return res.send(500, { error: err });
		return res.send(["confirmed"]);
	});
});
app.post('/friend/decline', function(req, res) {
	Contacts.remove({_id: req.body.roomId, people: req.user._id}, function(err) {
    	if (err) return res.send(500, { error: err });
		return res.send(["confirmed"]);
	});	
});
app.post('/friend/close', function(req, res) {
	Contacts.remove({roomName: req.body.roomId, people: req.user._id}, function(err) {
    	if (err) return res.send(500, { error: err });
		return res.send(["confirmed"]);
	});	
});
app.post('/friend/new', function(req, res) {
	var myName = req.user.name;
	var friendId = req.body.friendId;
	var friendName = '';
	var friendImg = '';
	var roomName = '';
    if (req.body.friendId == req.user._id) {
        //error - cannot add self
        if (err) return res.send(500, { error: 'Cannot add self.' });
    }
	//other name
	var frn = User.find({_id: friendId}).limit(1).lean();
	frn.exec(function(err, docs) {
		if (err) return next(err);
		docs = docs[0];
		//get the friend name
		friendName = docs.name;
		friendImg = docs.profilePicture;
        if (friendImg === undefined) {
            friendImg = 'defaultPic.png';
        }

		//make a room name
		roomName = (myName + friendName + randInt(12345).toString());
		roomName = roomName.replace(/[^0-9a-zA-Z]/g, '');

		//make a new contact
		var contact = new Contacts({
			people: [req.user._id, friendId],
			confirmed: false,
			timestamp: Math.floor(Date.now() / 1000),
			roomName: roomName
		});
		contact.save(function(err) {
			if (err) return res.send(500, { error: err });
			return res.send(['confirmed', roomName, friendName, friendImg]);
		});	
	});
});
app.post('/friend/report', function(req, res) {
	User.findOneAndUpdate({ _id: (req.body.id) }, { $inc: { reported: 1 }}).exec(function(err, doc) { 
		if (err) { 
			throw err; 
		} 
		return res.send(['confirmed']);
	});
});
app.post('/msg/initial/:room', function(req, res) {
	getMessages(req, res, false);
});

app.post('/msg/more/:room', function(req, res) {
	getMessages(req, res, true);
});

//socket config
io.use(function(socket, next){
	// Wrap the express middleware
	sessionMiddleware(socket.request, {}, next);
});
io.on("connection", function(socket) {
    //join a room with the url (takes a lot of parsing)
	var fullUrl = socket.request.headers.referer;
	var word = 'messages/';
	var regex = new RegExp('\\b' + word + '\\b');
	var pos = (fullUrl.search(regex)+word.length);
	var roomName = fullUrl.substring(pos, fullUrl.length);
	
	socket.join(roomName);
	
	//the first message date for loading more
	var firstMsgDate;
	
	var userId = 123;
	
	if (socket.request.session.passport) {
		userId = socket.request.session.passport.user;
	}
    
    //get messages from client
	socket.on("chatMessage", function(msg, clientRoom, msgId) {
		var sender = userId;
		var message = new Msg({
			sender: sender,
			body: msg,
			room: clientRoom
		});
		message.save(function(err) {
			if (err) throw err;
            if (msgId === undefined) {
                io.to(roomName).emit("chatMessage", msg, sender, convertUnix(Math.floor(Date.now() / 1000)));
            } else {
                io.to(roomName).emit("chatMessage", msg, sender, convertUnix(Math.floor(Date.now() / 1000)), msgId);
            }
		});	
	});
	
	ss(socket).on('message-media', function(stream, filename, clientRoom, previewImgId) {
        //TODO: implement some type of size check
		var name = createRandomName(filename, '');
		var nameSmall;
		stream.pipe(fs.createWriteStream("./public/uploads/messages/" + name));
		stream.on('finish', function() { 
			var format = name.substr(name.length - 4);
            //format = format.toLowerCase();
			if (format == '.mp4') {
				msg = '<video class="msgMedia" controls><source src="/uploads/messages/' + name + '" type="video/mp4">Your browser does not support the video tag.</video>';
			} else if (format == '.png' || format == '.jpg' || format == '.jpeg' || format == '.gif') {
				msg = '<img class="msgMedia" src="/uploads/messages/' + name + '"/>';
				nameSmall = createRandomName(filename, '.small');
				var cp = require('child_process');
				var child = cp.fork('./processes/image.js');
				// Send child process some work
				child.send([name, nameSmall]);
			} else {
				return false;
			}
			var sender = userId;
			var message = new Msg({
				sender: sender,
				media: [name, nameSmall],
				room: clientRoom
			});
			message.save(function(err) {
				if (err) throw err;
				io.to(roomName).emit("chatMessage", msg, sender, convertUnix(Math.floor(Date.now() / 1000)));
			});	
		});
	});
});

http.listen(3000, function() {
	cl("listening on *:3000");
});

//helper functions
function getMessages(req, res, isLoadMore) {
	var lastMsgId = '';
	if (isLoadMore) {
		lastMsgId = req.body.lastMsgId;
	}
	//new arrays to contain all info
	var messages = [];
	var date = [];
	var sender = [];
	
	//check if user is in room
	isInRoom(req.user._id, req.params.room, function(length) {
		if (length == 1) { //there is one result so they are in the room
			var m;
			if (isLoadMore) {
				//get messages where their timestamp is lower than current date - sort other way because this is confusing
				m = Msg.find({timestamp : {$lt : lastMsgId}, room : req.params.room}).sort({_id: -1}).limit(20); 
			} else {
				//get messages
				m = Msg.find({room : req.params.room}).sort({_id: -1}).limit(20); 
			}

			m.exec(function(err, message) {
				if (err) throw err;
				//go backwards through the messages so they are from oldest to newest whilst limits are still intact
				for(var i = 0; i < message.length; i++) {
					//make the first message date equal to the last document
					lastMsgId = message[i].timestamp;
					//add each info to its corresponding array
					if (message[i].media[0] !== undefined) {
						var format = message[i].media[0].substr(message[i].media[0].length - 4);
						if (format == '.mp4') {
							message[i].body = '<video class="msgMedia" controls><source src="/uploads/messages/' + message[i].media[0] + '" type="video/mp4">Your browser does not support the video tag.</video>';
						} else {
							message[i].body = '<img class="msgMedia small smallImg" src="/uploads/messages/' + message[i].media[1] + '" data-big="/uploads/messages/' + message[i].media[0] + '"/>';
						}
					}
					messages.push(message[i].body);
					date.push(convertUnix(message[i].timestamp));
					sender.push(message[i].sender);
				}
				//send the arrays over as well as the new first date so the button can update
				return res.send(['confirmed', messages, date, sender, lastMsgId]);
			});
		} else {
			return res.send(['error']);
		}
	});
}
function isInRoom(userId, room, callback) {
	var verify = Contacts.find({people: userId, roomName: room}).limit(1).lean();
	verify.exec(function(err, docs) {
		if (err) throw err;
		callback(docs.length);
	});
}
function is_mobile(req) {
    var ua = req.header('user-agent');
    if (/mobile/i.test(ua)) return true;
    else return false;
}
function getContacts(req, res, done) {
    var c = Contacts.find({people: req.user._id}).lean();
	c.exec(function(err, contacts) {
		if (err) throw err;
		var ids = [];
        for (var i=0; i<contacts.length; i++) {
            if (contacts[i].people[0] == req.user._id) {
                ids.push(mongoose.Types.ObjectId(contacts[i].people[1]));
            } else {
                ids.push(mongoose.Types.ObjectId(contacts[i].people[0]));
            }
        }
        var userPhoto = User.find({'_id': { $in : ids}}).lean();
        userPhoto.exec(function(err, photos) {
            photos = ids.map(function(id) {
                return photos.find(function(document) {
                    return document._id.equals(id);
                });
            });
            done(err, req.user, contacts, photos);
        });
	});
}
function createRandomName(originalName, extra) {
	var fileExt = originalName;
	fileExt = fileExt.substr(fileExt.lastIndexOf('.'), fileExt.length);
	var name = ((Date.now()/1000).toString() + '-' + randInt(12345).toString() + extra + fileExt);
	return name;
}
function randInt(num) {
	return (Math.floor(Math.random() * num));
}

function convertUnix(unixTimestamp) {
	return moment.unix(unixTimestamp).format("H:m:s - Do MMM YYYY");
}
function cl(string) {
	//this supports logging objects
    var clc = require('cli-color');
	str = JSON.stringify(string);
	str = JSON.stringify(string, null, 4); // (Optional) beautiful indented output.
	console.log(clc.green(str));
}
