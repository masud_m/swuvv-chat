module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		appPaths: {
			app: 'build/dist',
			dist: 'build/dist',
			source: '.'
		},
		clean: {
		  	dist: '<%= appPaths.dist %>'
		},
		copy: {
			dist: {
				expand: true,
				cwd: '',
				dest: '<%= appPaths.dist %>',
				src: [ '**' , '!node_modules/**', 
					  '!**/messages/**',
					  '!**/profile-picture/**',
					  '!**/build/**',
					  '!Gruntfile.js',
					  '!plan.txt',
					  'public/uploads/messages/defaultPic.png',
					  'public/uploads/profile-picture/defaultPic.png']
			}
		},
		jshint: {
			options: {
				reporter: require('jshint-stylish') // use jshint-stylish to make our errors look and read good
			},
			// when this task is run, lint the Gruntfile and all js files in src
			build: ['<%= appPaths.source %>/public/js/*.js', 
					'!<%= appPaths.source %>/public/js/socketio-stream.js', 
					'!<%= appPaths.source %>/public/js/jquery.js', 
					'!<%= appPaths.source %>/public/js/moment.js', 
					'!<%= appPaths.source %>/public/js/jquery-ui.js'],
			gruntfile: ['Gruntfile.js'],
			server: ['<%= appPaths.source %>/app.js', 
					 '<%= appPaths.source %>/models/*.js', 
					 '<%= appPaths.source %>/processes/*.js', 
					 '<%= appPaths.source %>/auth/*.js']
		},
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: '',
					src: ['<%= appPaths.dist %>/public/css/*.css'],
					dest: '',
					ext: '.css'
				}]
			}
		},
		uglify: {
			options: {
				banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
			},
			public: { 
				src: ['<%= appPaths.dist %>/public/js/*.js'],  // source files mask
				dest: '<%= appPaths.dist %>/public/js/',    // destination folder
				expand: true,    // allow dynamic building
				flatten: true,   // remove all unnecessary nesting
				ext: '.js'   // replace .js to .min.js
			},
			models: { 
				src: ['<%= appPaths.dist %>/models/*.js'],  // source files mask
				dest: '<%= appPaths.dist %>/models/',    // destination folder
				expand: true,    // allow dynamic building
				flatten: true,   // remove all unnecessary nesting
				ext: '.js'   // replace .js to .min.js
			},
			auth: { 
				src: ['<%= appPaths.dist %>/auth/*.js'],  // source files mask
				dest: '<%= appPaths.dist %>/auth/',    // destination folder
				expand: true,    // allow dynamic building
				flatten: true,   // remove all unnecessary nesting
				ext: '.js'   // replace .js to .min.js
			},
			processes: { 
				src: ['<%= appPaths.dist %>/processes/*.js'],  // source files mask
				dest: '<%= appPaths.dist %>/processes/',    // destination folder
				expand: true,    // allow dynamic building
				flatten: true,   // remove all unnecessary nesting
				ext: '.js'   // replace .js to .min.js
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	
	grunt.registerTask('default', [
		'jshint',
		'clean',
		'copy',
		'uglify',
		'cssmin'
    ]);
};