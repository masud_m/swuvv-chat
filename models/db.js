var mongoose = require('mongoose');
var vals = require('../const');
mongoose.Promise = global.Promise;
mongoose.connect(vals.mongo);

exports.mongoose = function() {
	return mongoose;
};