var db = require('./db');
var mongoose = db.mongoose();
var Schema = mongoose.Schema;


// create User Schema
var User = new Schema({
	provider: String,
	name: String,
	serviceID: String,
	email: String,
	profilePicture: String,
	reported: Number, //amount of time reported
	info: {
		bio: String,
		dob: {
			value: String,
			view: Boolean,
			decade: Number
		},
		gender: {
			value: String,
			view: Boolean
		},
		media: {
			view: Boolean,
			facebook: String,
			twitter: String,
			instagram: String,
			snapchat: String,
			youtube: String,
			linkedin: String
		}
	}
});


module.exports = mongoose.model('users', User);