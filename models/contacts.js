var db = require('./db');
var mongoose = db.mongoose();
var Schema = mongoose.Schema;

// create a schema
//alowed schema types: String, Number, Date, Buffer, Boolean, Mixed, ObjectId, Array
var contactsSchema = new Schema({
    people: { type: [String], index: true }, //the people id's that are in the chat, first person is the person that initiated
	confirmed: Boolean,
    timestamp: Number,
	roomName: { type: String, index: true }
});

// on every save, add the date
contactsSchema.pre('save', function(next) {
  // get the current date
  var currentDate = Math.floor(Date.now() / 1000);
  //change the timestamp field to current date
  this.timestamp = currentDate;
  next();
});

// the schema is useless so far
// we need to create a model using it
var contact = mongoose.model('Contact', contactsSchema);

// make this available to our users in our Node applications
module.exports = contact;