var db = require('./db');
var mongoose = db.mongoose();
var Schema = mongoose.Schema;

// create a schema
//alowed schema types: String, Number, Date, Buffer, Boolean, Mixed, ObjectId, Array
var messagesSchema = new Schema({
    sender: String,
    body: String,
    timestamp: { type: Number, index: true },
	room: { type: String, index: true },
	media: { type: [String] } //first one is normal, second is small
});

// on every save, add the date
messagesSchema.pre('save', function(next) {
  // get the current date
  var currentDate = Math.floor(Date.now() / 1000);
  //change the timestamp field to current date
  this.timestamp = currentDate;
  next();
});

// the schema is useless so far
// we need to create a model using it
var message = mongoose.model('Message', messagesSchema);

// make this available to our users in our Node applications
module.exports = message;