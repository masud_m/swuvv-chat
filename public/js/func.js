//remember scroll pos of sidebar
$(".sidebar").scroll(function() { 
	var tempScrollTop = $('.sidebar').scrollTop();
	localStorage.setItem("scroll", tempScrollTop);
});
$('.sidebar').scrollTop(localStorage.getItem("scroll"));

//goto function
function goto(place) {
	window.location = place;
}

//quick console log
function cl(string) {
	console.log(string);
}

//quick document.getElementById
function _(id) {
	if (document.getElementById(id) !== null) {
		return document.getElementById(id);
	}
}

function gotoBottom(div, length) {
	div.stop().animate({
	  scrollTop: div[0].scrollHeight
	}, length);
}

function clickHidden(el) {
	document.getElementById(el).click();
}

function hideScroll(div) {
	setTimeout(
    function() {
      $(div).css('overflow', 'auto');
    }, 75);
}
