//get a name - provided from the actual messages page
//var name = prompt("enter a name");
//if they do not provide a name, loop until they do
/*while (name == "") {
	var name = prompt("enter a name");
}*/

//divs
var messages = $("#messages"); //messages

//start socket with default namespace
var socket = io();

//can the pop up be disabled
var canPopupBeDisabled = false;

$(window).on('load', function() {
    //executes when complete page is fully loaded, including all frames, objects and images
	//get the first messages
	setTimeout(function(){ 
		$.post("/msg/initial/" + clientRoom, {room:clientRoom} ,function(data){
			getMessages(data, false);
		});
	}, 700);
});

//on form submit
$("form").submit(function() {
	//get a message
	var msg = $("#m").val(); 
	//if the message is null, warn the user
	if (msg === "") {
		alert("enter message!"); 
		return false;
	}
	//emit the message
    var id = Math.floor(Math.random()*1000);
    var currentdate = new Date(); 
    var datetime = currentdate.getHours() + ":" + currentdate.getMinutes() + ":"+ currentdate.getSeconds() + ' - '+ currentdate.getDate() + "/" + (currentdate.getMonth()+1)  + "/"+ currentdate.getFullYear();
	socket.emit("chatMessage", msg, clientRoom, id);
    newMessage(msg, name, datetime, id);
	//empty the message box
	$("#m").val('');
	//do not submit the form (refesh the page)
	return false;
});

//when a message is recieved do the following - requires the msg and sender
socket.on("chatMessage", function(msg, sender, date, msgId){
    if (msgId === undefined) {
        newMessage(msg, sender, date); 
    } else {
        if (_('msg'+msgId)) {
            document.getElementById('msg'+msgId).className = "hide";
        } else {
			newMessage(msg, sender, date); 
		}
    }
	
});

//load more messages
function loadMore(lastMsgId) { //get x messages before id parameter
	$.post("/msg/more/" + clientRoom, {room: clientRoom, lastMsgId: lastMsgId}, function(data){
		getMessages(data, true);
	});
}

function getMessages(data, isLoadMore) {
	if(data[0] === 'confirmed') {
		_('loadingMsgs').className = 'hide';
		msgArray = data[1];
		dateArray = data[2];
		senderArray = data[3];
		var firstMsg, curOffset;
		
		if (isLoadMore) {
			//disable load more if there are no more results
			if (msgArray.length < 1) {
				_("loadMoreBtn").onclick = function() { return false; };
				_("loadMoreBtn").className = "hide";
				return false;
			}
			// Store reference to top message
			firstMsg = $('.bubble:first');
			// Store current scroll/offset
			curOffset = firstMsg.offset().top - messages.scrollTop();	
		} else {
			//initial - to get the most recent messages not the load more ones
			if (msgArray.length < 1) {	
				_("welcomeMsg").className = "button grey preset middle no-click topMargin"; //give it all these attributes, should make it look nice
				return false;
			}
			if (msgArray.length < 20) {
				$('#loadMoreBtn').remove();
			} else {
				_("loadMoreBtn").className = "button red preset middle topMargin";
			}
		}
		
		//go through array
		for (var i=0;i<msgArray.length;i++) {
			sender = senderArray[i];
			if (sender == name) {
				messages.prepend($('<div class="bubble ownMsg blue"><div class="ownMsg blue">' + msgArray[i] + '</div></div><div class="msgDate">' + dateArray[i] + '</div><div class="clear"></div>'));
			} else {
				messages.prepend($('<div class="bubble"><div class="">' + msgArray[i] + '</div></div><div class="msgDateLeft">' + dateArray[i] + '</div><div class="clear"></div>'));
			}
			
		}
		if (_("loadMoreBtn")) {
			_("loadMoreBtn").onclick = function() { loadMore(data[4]); };
			$("#loadMoreBtn").prependTo(messages);
		}
        
        setTimeout(function() {
            var images = document.getElementsByClassName('smallImg');
            for(var i = 0; i < images.length; i++) {
                images[i].src = images[i].getAttribute('data-big');
                images[i].className = 'msgMedia smallImg';
            }
        }, 500);

		if (isLoadMore) {
			// Set scroll to current position minus previous offset/scroll
			messages.scrollTop(firstMsg.offset().top-curOffset);
		} else {
			gotoBottom(messages, 0);
			//initial messages
			$(document).ready(function(){
				setTimeout(function(){ gotoBottom(messages, 0); }, 600);
			});
		}
	} else {
		_('loadingMsgs').innerHTML = 'Error.';
	}
	
	
}

function newMessage(msg, sender, date, msgId) {
	if (canPopupBeDisabled) {
		openPopup('hide');
	}
    if (msgId === undefined) {
        msgId = '';
    } else {
        msgId = '<span class="show" id="msg'+msgId+'">Sent</span>';
    }
    if (sender == name) {
        messages.append($('<div class="bubble ownMsg blue"><div class="ownMsg blue">' + msg + '</div></div><div class="msgDate">' + date + msgId + '</div><div class="clear"></div>'));
    } else {
        messages.append($('<div class="bubble"><div class="">' + msg + '</div></div><div class="msgDateLeft">' + date + '</div><div class="clear"></div>'));
		beep();
    }
	$(document).ready(function(){
		if(messages.scrollTop() > messages.height() - 10) {
			gotoBottom(messages, 500);
			setTimeout(function(){ gotoBottom(messages, 500); }, 500);
		}
	});
}

//disable enter in textarea
$(".msgInput").keydown(function(e){
	if (e.keyCode == 13 && !e.shiftKey) {
		// prevent default behavior
		e.preventDefault();
		$("form").submit();
		return false;
	}
});

function openMessageFileInput(e) {
	e.preventDefault();
	_('msgOther').click();
}

$('#msgOther').change(function(e) {	
	if (showImage() !== false) {
		openPopup('show');
	}
});

function openPopup(action) {
	_('backgroundPopup').className = action;
	_('popup').className = action;
	$('#popupImg').attr('src', null);
	canPopupBeDisabled = false;
    _('loadingCircle').className = 'hide';
    _('popupVid').className = 'hide';
    _('progressAmount').className = 'hide';
    _('progressAmount').innerHTML = '0%';
}

function confirmPopup() {
	_('popupVid').className = 'hide';
	_('popupImg').className = 'hide';
	_('loadingCircle').className = 'show';
    _('progressAmount').className = 'show';
	$('#popupImg').attr('src', null);
	var file = _('msgOther').files[0];
	var stream = ss.createStream();

	// upload a file to the server. 
	ss(socket).emit('message-media', stream, file.name, clientRoom);
	var blobStream = ss.createBlobReadStream(file);

	var size = 0;
	blobStream.on('data', function(chunk) {
	  size += chunk.length;
	  //console.log(Math.floor(size / file.size * 100) + '%');
	  // -> e.g. '42%' 
		percent = Math.floor(size / file.size * 100);
        _('progressAmount').innerHTML = percent + '%';
		if (percent > 99) {
			canPopupBeDisabled = true;
		}
		
	});

	blobStream.pipe(stream);
}

function declinePopup() {
	openPopup('hide');
}

function showImage() {
	e = _('msgOther');
	if (e.files && e.files[0]) {
        format = '';
        if (e.files[0].type.match('video/*')) {
            // file type starts with text
            format = 'vid';
        } else if (e.files[0].type.match('image/*')) {
            // file type does not start with text
            format = 'img';
        } else {
            alert('Not an image or video.');
            return false;
        }
        var filesize = (e.files[0].size/1000000);
        if (filesize < 15) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (format == 'img') {
                    $('#popupImg').attr('src', e.target.result);
                } else if (format == 'vid') {
                    $('#popupVid').attr('src', e.target.result);
                    _('popupVid').className = 'show';
                }
            };
            reader.readAsDataURL(e.files[0]);
        } else {
            alert('File too big.');
            return false;
        }
        
    }
}

function report(id) {
	_('reportBtn').innerHTML = 'Reporting...';
	$.post("/friend/report", {id:id} ,function(data){
		if (data[0] == 'confirmed') {
			_('reportLink').onclick = function() { return false; };
			_('reportBtn').innerHTML = 'Reported';
		}
	});
}
function closeConversation(id) {
	var conf = confirm("Are you sure you want to close this conversation?");
	if (conf === true) {
		_('closeBtn').innerHTML = 'Closing...';
		$.post("/friend/close", {roomId:id} ,function(data){
			if (data[0] == 'confirmed') {
				location.reload();
			}
		});
	}	
}

function beep() {
	var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");  
	snd.play();
}

$( ".cross" ).hide();
$( ".menu" ).hide();
$( ".hamburger" ).click(function() {
$( ".menu" ).slideToggle( "75", function() {
$( ".hamburger" ).hide();
$( ".cross" ).show();
});
});

$( ".cross" ).click(function() {
$( ".menu" ).slideToggle( "75", function() {
$( ".cross" ).hide();
$( ".hamburger" ).show();
});
});