function confirmFriend(roomId) {
	$.post("/friend/confirm", {roomId: roomId}, function(data){
		if(data[0] === 'confirmed') {
			location.reload();
		}
	});
}
function declineFriend(roomId) {
	$.post("/friend/decline", {roomId: roomId}, function(data){
		if(data[0] === 'confirmed') {
			window.location = "/";
		}
	});
}
function viewFriend(id, display) {
	_('backgroundPopup').className = display;
	_('popup.'+id).className = display;
}
function addFriend(id) {
	$.post("/friend/new", {friendId: id}, function(data){
		if(data[0] === 'confirmed') {
			if (_('sidebar')) {
				_('sidebar').innerHTML += '<div class="contact blue redBorder redGlow button" onclick="goto(\'/messages/'+data[1]+'\');"><img class="contactImg" src="/uploads/profile-picture/'+data[3]+'"/><span class="contactName"> Confirm Friend - '+data[2]+'</span></div>';
                gotoBottom($('.sidebar'), 500);
				viewFriend(id, 'hide');
			} else {
                window.location = '/';
            }
		}
	});
}
function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
function convertAge() {
	var id = document.getElementsByClassName('popupAge');
	for (var i = 0; i < id.length; i++) {
		id[i].innerHTML = 'Age: ' + getAge(id[i].innerHTML);
	}
}