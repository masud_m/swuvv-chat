$( function() {
	if ($( "#datepicker" ).length) {
		$( "#datepicker" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-150:+0",
			dateFormat: "dd-M-yy"
		});
	}
});
function setupSave() {
	var dob = _('datepicker').value;
	if (dob === undefined || dob === '') {
		alert('Please enter your date of birth!');
		return false
	}
	
	var gender = _('gender').value;
	if (gender === undefined || gender === '') {
		alert('Please enter your gender!');
		return false;
	}
	
	var bio = _('setupBio').value;
	if (bio === undefined || bio === '') {
		alert('Please enter something in your bio!');
		return false;
	}
	if (saveEverything('setup')) {
		window.location = '/';
	}
}
function saveEverything(pageType) {
	var confirm = true;
	var bio;
	if (pageType == 'setup'){
		bio = _('setupBio').value;
	} else {
		bio = _('settingsBio').value;
	}
	if (bio !== undefined) {
		if (bio.length < 401) {
			$.post("/settings/bio", {'bio': bio}, function(data){
				if(data[0] !== 'confirmed') {
					confirm = false;
				}
			});
		} else {
			alert('Bio is too long. Max 400 characters');
			return false;
		}
	}
	var dob = _('datepicker').value;
	var dobView = false;
	if (_('dobView').value == 'Public') {
		dobView = true;
	} 
	if (dob !== undefined && dob !== '') {
		dobM = moment(dob, "DD-MMM-YYYY");
		dobMin = moment(moment().subtract("18", 'years').calendar(), "MM/DD/YYYY");
		if (!dobM.isBefore(dobMin)) {
			alert('You have to be 18 to use this site.');
			confirm = false;
			return false;
		}
		decade = (Math.round($.datepicker.formatDate('yy', $('#datepicker').datepicker('getDate')) / 10) * 10);
		$.post("/settings/age", {'dob': dob, 'dobView':dobView, 'decade': decade}, function(data){
			if(data[0] !== 'confirmed') {
				confirm = false;
			}
		});
	}
	var gender = _('gender').value;
	var genderView = false;
	if (_('genderView').value == 'Public') {
		genderView = true;
	} 
	if (gender !== undefined) {
		$.post("/settings/gender", {'gender': gender, 'genderView':genderView}, function(data){
			if(data[0] !== 'confirmed') {
				confirm = false;
			}
		});
	}
	
	if (pageType != 'setup') {
		var mediaView = false;
		if (_('mediaView').value == 'Public') {
			mediaView = true;
		} 
		$.post("/settings/media", {'mediaView': mediaView, 'fb': _('fb').value, 'tw': _('tw').value, 'sc': _('sc').value, 'ig': _('ig').value, 'li': _('li').value, 'yt': _('yt').value}, function(data){
			if(data[0] !== 'confirmed') {
				confirm = false;
			}
		});
	}
	
	_('saveLoading').className = 'show';
	uploadProfilePicture(function() {
		if (confirm === true) {
			glowButton(2);
			_('saveLoading').className = 'hide';
		}
	});
	
	return true;
}
function uploadProfilePicture(callback) {
	var fd = new FormData();  
	profilePic = _('profilePic').files[0];
	if (profilePic === undefined) {
		callback();
		return false;
	}
	var pType = profilePic.type;
	if (pType != "image/jpg" && pType != "image/jpeg" && pType != "image/png" && pType != "image/gif") {
		alert('Only images allowed.');
		return false;
	}
	fd.append('profile-picture', profilePic);
	var xhr = new XMLHttpRequest();
	
	// your url upload
	xhr.open('post', '/upload/profile-picture', true);

	xhr.upload.onprogress = function(e) {
		if (e.lengthComputable) {
			var percentage = Math.floor((e.loaded / e.total) * 100);
			_('saveProgress').innerHTML = (percentage + "%");
		}
	};

	xhr.onerror = function(e) {
		console.log('Error');
		console.log(e);
	};
	
	xhr.onload = function() {
		data = JSON.parse(this.responseText);
		if (data[0] === 'confirmed') {
			_('currProfilePic').src = '/uploads/profile-picture/' + data[1];
			callback();
			return true;
		}
	};

	xhr.send(fd);
}
$('#profilePic').change(function(e) {
	loadImage();
	glowButton(1);
});
function loadImage() {
	e = _('profilePic');
	if (e.files && e.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#currProfilePic').attr('src', e.target.result);
        };
        reader.readAsDataURL(e.files[0]);
    }
}

function glowButton(mode) {
	if (mode == 1) {
		$('#saveProfile').addClass('red');
		$('#saveProfile').addClass('redGlow');
		$('#saveProfile').addClass('transition');
		$('#saveProfile').removeClass('redBorder');
	} else if (mode == 2) {
		$('#saveProfile').removeClass('red');
		$('#saveProfile').removeClass('redGlow');
		$('#saveProfile').addClass('redBorder');
	}
}

$("#settingsBio").keyup(function(){
    glowButton(1);
});
$("#setupBio").keyup(function(){
    glowButton(1);
});
$("#datepicker").on('change', function() {
	glowButton(1);
});
$("#dobView").on('change', function() {
	glowButton(1);
});
$("#genderView").on('change', function() {
	glowButton(1);
});
$("#gender").on('change', function() {
	glowButton(1);
});
$("#mediaView").on('change', function() {
	glowButton(1);
});
$("#fb").keyup('change', function() {
	glowButton(1);
});
$("#tw").keyup('change', function() {
	glowButton(1);
});
$("#ig").keyup('change', function() {
	glowButton(1);
});
$("#sc").keyup('change', function() {
	glowButton(1);
});
$("#yt").keyup('change', function() {
	glowButton(1);
});
$("#li").keyup('change', function() {
	glowButton(1);
});

